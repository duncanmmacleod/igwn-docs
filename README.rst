IGWN Documentation Index
========================

This Git repository contains the source code for the IGWN Sphinx documentation
index on readthedocs. For the latest HTML edition, visit
http://igwn.readthedocs.io/.

Building
--------

To render the HTML version of the User Guide on your own computer, make sure
that you have a working Python environment with Python >= 3.5. Then run the
following commands::

    python -m venv env
    source env/bin/activate
    git clone https://git.ligo.org/docs/igwn.git
    cd igwn
    pip install -r requirements.txt
    make html

Then open the main page _build/html/index.html in your favorite browser.
